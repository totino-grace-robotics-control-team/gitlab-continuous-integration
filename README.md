This CI/CD file contains the necessary stages to build robot code and java challenges.

It can:
 - build robot code
 - run robot tests
 - review robot code
 - deploy robot code to roborios
 - merge robot code when ready
